#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 12:29:21 2022

@author: pthuringer
"""

from Tour import Tour

class Duel:
    
    def __init__(self,sorcier1, sorcier2):
        self.sorcier1 = sorcier1
        self.sorcier2 = sorcier2
        self.tourCourant = Tour(1,sorcier1,sorcier2) 
        self.tours=[self.tourCourant]
        self.etat = "en cours"
    
        
        #initialisation de tourCourant avec la création d’une instance de Tour       
        #initialisation du premier élément du tableau tours avec le tour courant
        #initialisation de l’état du seul à « en cours »
    

    def getTourCourant(self):
        return self.tourCourant
    
    
    def getEtat(self):
        return self.etat
       
    
    def setEtat(self,etat):
        self.etat =etat
     
    def tourSuivant(self):
        #calcule le numero du tour suivant à partir du tour courant
        numToursSuivant = self.getTourCourant().getNumero()
        #instancie un nouveau Tour avec le numéro suivant
        tourSuivant = Tour(numToursSuivant,self.sorcier1,self.sorcier2)
        #affecte le nouveau tour à tourCourant
        self.tourCourant=tourSuivant;
        #ajout du nouveau tour à au tableau tours
        self.tours.append(self.tourCourant)
    
    def determinerVainqueur(self):
        #score du tour courant
        score =self.tourCourant.getScore()
        #test les valeurs des points de vie stockés dans le score et affiche le résultat (ne pas oublier le match nul)
        if(score[0]>0 and score[1]<=0):
            print("adversaire remporte le duel")
        elif(score[0]<=0 and score[1]<=0):
             print("match null")
        elif(score[0] <=0 or score[1]<=0):
            print("tour terminé")
        else:
            print("le tour se termine et un autre commence")
            
    #def afficherResumeDuel(self):
        #affiche : Le duel est terminé. Résumé du duel :
            #print("Le duel est terminé. Résumé du duel")
        #pour chaque tour stocké dans la tableau tours, appelle afficheResumeTour()
           # for tour in self.tours :
                #tour.afficheResumeTour()

        #appelle determinerVainqueur
            #self.determinerVainqueur()
        
       
        
       
        #if(score[0]<=0 or score[1]<=0):
            #print("sorcier1" + score[1] +"sorcier2" +score[2] +"match terminé")
        
       
           # if(score[0]<=0 and score[1]<=0):
               # print("sorcier1" + score[1] +"sorcier2" +score[2] +"match null")
            #elif(score[0]<=0 and score[1]>0):
                #print("sorcier1" + score[1] +"sorcier 2" +score[2] +"joueur 2 remporte le match")
            #elif(score[0]>0 and score[1]<=0):
                #print(score[1] + str("sorcier1") + score[2] +str("sorcier 2") + "joueur 1 remporte le match")
            
    
    def afficherResumeDuel(self):
        #affiche : Le duel est terminé. Résumé du duel :
            print("Le duel est terminé. Résumé du duel")
        #pour chaque tour stocké dans le tableau tours, appelle afficheResumeTour()
            for tour in self.tours :
                self.afficheResumeTour=('afficheResumeTour')
                
        #appelle determinerVainqueur
            self.determinerVainqueur()