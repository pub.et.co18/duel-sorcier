#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 12:12:55 2022

@author: pthuringer
"""


class Sort:

    def __init__(self,nom, type, pdd, pdr):
        self.nom = nom
        self.type = type
        self.pdd = pdd
        self.pdr = pdr
        
        #initialise le nom du sort
        #initialise le type du sort
        #initialise le nombre de points de dégâts du sort
        #initialise le nombre de points de récupération du sort
    
    def getNom(self):
        return self.nom
    
        #retourne le nom du sort
    
    def getType(self):
        return self.type
    
        #retourne le type du sort
    
    def getPDD(self):
        return self.pdd
        #retourne le nombre de points de dégâts du sort
    
    def getPDR(self):
        return self.pdr 
         
    
        #initialise le nombre de points de récupération du sort
    
