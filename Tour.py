#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 12:18:28 2022

@author: pthuringer
"""

from SortOffensif import SortOffensif
from SortDefensif import SortDefensif
from SortSournois import SortSournois


class Tour:
    
    def __init__(self,numero, sorcier1, sorcier2):
        #initialise le numero du tour
        self.numero = numero
        #initialise la liste sorcier avec les deux sorciers 
        self.sorcier =[sorcier1,sorcier2]
        #initialise le score à 0,0
        self.score =[0,0]
        self.sort=[0,0]
        #affiche : # Tour [numero du tour] # 
        print(self.numero)
    

    def getNumero(self):
        #retourne le numero du tour
        return self.numero
    

    def getScore(self):
        #retourne le score en fin de tour
        return self.score

    def getSorciers(self):
        #retourne la liste des sorciers participant au tour
        return self.sorcier
    

    def tourSorcier1(self):
        #affiche : - [nom du sorcier 1]:
        print(self.sorcier[0].getNom())
        # Teste si le numero du tour est 1 ou 4,7,10 etc c’est-à-dire si numero%3=1 -> Dans ce cas le sorcier 1 doit lancer un sort offensif
        
    
        if(self.numero%3==1):
       
            while(True):
                #demande de saisie utilisateur avec le message : Choisir un sort offensif: 1 pour Flipendo, 2 pour Impedimenta, 3 pour Crache limace :
                numeroSortALancer = int(input("choisissez un sort: 1 pour Fipendo , 2 pour Impedimenta, 3 pour Crache limace :"))
                
                #Cas où la saisie est :
                if(numeroSortALancer == 1):
                       
                       #instancie le sort offensif Flipendo avec 4 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0] = SortOffensif("Flipendo",4)
                       break
                elif(numeroSortALancer == 2) :
                       #instancie le sort offensif Impedimenta avec 5 points de dégâts
                       #ajoute le sort à la liste sorts
                        self.sort[0] = SortOffensif("Impedimenta",5)
                        break
                    
                    #case 3 : instancie le sort offensif Crache Limace avec 5 points de dégâts
                elif(numeroSortALancer == 3) :
                       #ajoute le sort à la liste sorts
                       self.sort[0]= SortOffensif("Crache Limace",5)
                       break
                    #autre saisie : affiche « Mauvaise saisie, veuillez recommencer. »
                else:
                    print(" Mauvaise saisie, veuillez recommencer.")
                
             
            
        #Teste si le numero du tour est 2 ou 3,8,11 etc c’est-à-dire si numero%3=2 -> Dans ce cas le sorcier 1 doit lancer un sort sournois
        elif(self.numero%3==2):
       
            while(True):
                #demande de saisie utilisateur avec le message : Choisir un sort sournois: 1 pour Legilimens, 2 pour Morsmordre, 3 pour Oubliette :
                numeroSortALancer = int(input("choisissez un sort surnois: 1 pour Legilimens, 2 pour Morsmordre, 3 pour Oubliette :" ))
                    #Cas où la saisie est :
                if(numeroSortALancer ==1):
                       #instancie le sort sournois Legilimens avec 5 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0]= SortSournois("Legilimens",5)
                       break
                elif(numeroSortALancer ==2 ):
                       #instancie le sort sournois Morsmordre avec 3 points de dégâts
                       #ajoute le sort à la liste sorts
                        self.sort[0]=SortSournois("Morsmordre",3)
                elif(numeroSortALancer ==3):
                    #3 : instancie le sort sournois Oubliettes avec 4 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0] = SortSournois("Oubliettes",4)
                       break
                    #autre saisie : affiche « Mauvaise saisie, veuillez recommencer »
                else:
                    print("Mauvaise saisie, veuillez recommencer ")
           
       # Teste si le numero du tour est 3 ou 6,9,12 etc c’est-à-dire si numero%3=0 -> Dans ce cas le sorcier 1 doit lancer un sort défensif
        elif(self.numero%3==0):
    
            while(True):
                
                #demande de saisie utilisateur avec le message : Choisir un sort défensif : 1 pour Expelliarmus, 2 pour Protego, 3 pour Spero Patronum :
                numeroSortALancer = int(input("Choisir un sort défensif : 1 pour Expelliarmus, 2 pour Protego, 3 pour Spero Patronum :"))
                    #Cas où la saisie est :
                if(numeroSortALancer ==1):
                       #instancie le sort défensif Expelliarmus avec 5 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0] = SortDefensif("Expelliarmus",5)
                       break
                elif(numeroSortALancer ==2):
                       #instancie le sort défensif Protego avec 3 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0]=SortSournois("Protego",3)
                       break
                elif(numeroSortALancer ==3):
                    #3 : instancie le sort défensif Spero Patronum avec 4 points de dégâts
                       #ajoute le sort à la liste sorts
                       self.sort[0] = SortDefensif("Spero Patronum",4)
                       break
                else:
                          print("Mauvaise saisie, veuillez recommencer ")
     
          
        #le sorcier 1 lance le sort choisi
        self.sorcier[0].lancerSort(self.sort[0])
    

    def tourSorcier2(self):
     #instructions similaires à tourSorcier1() mais pour le sorcier 2, attention les types de sort diffèrent du sorcier 1 pour le même numéro de tour cf. Règles de combat: Ex: au tour 1 le sorcier 1 lance un sort offensif et le sorcier 2 répond par un sort défensif, etc     
             print(self.sorcier[1].getNom())
             if(self.numero%3==1):
    
                while(True):
                    
                    #demande de saisie utilisateur avec le message : Choisir un sort défensif : 1 pour Expelliarmus, 2 pour Protego, 3 pour Spero Patronum :
                    numeroSortALancer = int(input("Choisir un sort défensif : 1 pour Expelliarmus, 2 pour Protego, 3 pour Spero Patronum :"))
                        #Cas où la saisie est :
                    if(numeroSortALancer ==1):
                           #instancie le sort défensif Expelliarmus avec 5 points de dégâts
                           #ajoute le sort à la liste sorts
                           self.sort[1] = SortDefensif("Expelliarmus",4)
                           break
                    elif(numeroSortALancer ==2):
                           #instancie le sort défensif Protego avec 3 points de dégâts
                           #ajoute le sort à la liste sorts
                           self.sort[1]=SortDefensif("Protego",3)
                           break
                    elif(numeroSortALancer ==3):
                        #3 : instancie le sort défensif Spero Patronum avec 4 points de dégâts
                           #ajoute le sort à la liste sorts
                           self.sort[1] = SortDefensif("Spero Patronum",4)
                           break
                    else:
                              print("Mauvaise saisie, veuillez recommencer ")
             self.sorcier[1].lancerSort(self.sort[1])
        
             if(self.numero%3==2):
                  while(True):
                        #demande de saisie utilisateur avec le message : Choisir un sort offensif: 1 pour Flipendo, 2 pour Impedimenta, 3 pour Crache limace :
                        numeroSortALancer = int(input("choisissez un sort offensif: 1 pour Fipendo , 2 pour Impedimenta, 3 pour Crache limace :"))
                        
                        #Cas où la saisie est :
                        if(numeroSortALancer == 1) :
                               
                               #instancie le sort offensif Flipendo avec 4 points de dégâts
                               #ajoute le sort à la liste sorts
                               self.sort[1] = SortOffensif("Flipendo",3)
                               break
                        elif(numeroSortALancer == 2) :
                               #instancie le sort offensif Impedimenta avec 5 points de dégâts
                               #ajoute le sort à la liste sorts
                                self.sort[1] = SortOffensif("Impedimenta",5)
                                break
                            #case 3 : instancie le sort offensif Crache Limace avec 5 points de dégâts
                        elif(numeroSortALancer == 3) :
                               #ajoute le sort à la liste sorts
                               self.sort[1]= SortOffensif("Crache Limace",5)
                               break
                            #autre saisie : affiche « Mauvaise saisie, veuillez recommencer. »
                        else:
                            print(" Mauvaise saisie, veuillez recommencer.")
             elif(self.numero%3==0):
       
                while(True):
                    #demande de saisie utilisateur avec le message : Choisir un sort sournois: 1 pour Legilimens, 2 pour Morsmordre, 3 pour Oubliette :
                    numeroSortALancer =int( input("choisissez un sort surnois: 1 pour Legilimens, 2 pour Morsmordre, 3 pour Oubliette :" ))
                        #Cas où la saisie est :
                    if(numeroSortALancer ==1):
                           #instancie le sort sournois Legilimens avec 5 points de dégâts
                           #ajoute le sort à la liste sorts
                           self.sort[1]= SortSournois("Legilimens",5)
                           break
                    elif(numeroSortALancer ==2 ):
                           #instancie le sort sournois Morsmordre avec 3 points de dégâts
                           #ajoute le sort à la liste sorts
                            self.sort[1]=SortSournois("Morsmordre",3)
                    elif(numeroSortALancer ==3):
                        #3 : instancie le sort sournois Oubliettes avec 4 points de dégâts
                           #ajoute le sort à la liste sorts
                           self.sort[1] = SortSournois("Oubliettes",4)
                           break
                        #autre saisie : affiche « Mauvaise saisie, veuillez recommencer »
                    else:
                        print("Mauvaise saisie, veuillez recommencer ")
                        
                        
        
       
    def calculerPDV(self):

        #Type du sort lancé par le sorcier 1
        typeDeSortSorcier1 =self.sort[0].getType() 
        #Type du sort lancé par le sorcier 2
        typeDeSortSorcier2 =self.sort[1].getType() 
        # Test du cas d'un sort offensif contré par un sort défensif
        if(typeDeSortSorcier1 =="offensif" and typeDeSortSorcier2=="defensif" or
           typeDeSortSorcier1=="sournois"  and typeDeSortSorcier2=="offensif"):
            #points de dégât du sort du sorcier 1
            pddSor1 = self.sort[0].getPDD()
            #application des points de dégâts sur la jauge de pdv du sorcier 2, appel de perdrePDV()
            self.sorcier[1].perdrePDV(pddSor1)
            print(pddSor1)
            print(self.sorcier[1].getPDV())
            #points de récupération du sort du sorcier2
            pddSor2 = self.sort[1].getPDR()
            #application des points de récupération sur la jauge du sorcier 2, appel de recupererPDV()
            self.sorcier[1].recupererPDV(pddSor2)
            self.score[0]=self.sorcier[0].getPDV()
            self.score[1]=self.sorcier[1].getPDV()
        # test du cas d'un sort sournois contré par un sort offensif
        elif(typeDeSortSorcier1 =="sournois" and typeDeSortSorcier2=="offensif"):
            # or typeDeSortSorcier1=="surnois" and typeDeSortSorcier2=="offensif"):

            #point de dégâts du sort du sorcier 1
             pddSor1 = self.sort[0].getPDD()
            #application des point de dégâts sur la jauge du sorcier 2, appel de perdrePDV()
             self.sorcier[1].perdrePDV(pddSor1)
            #points de dégâts du sort du sorcier 2
             pddSor2 = self.sort[1].getPDD()
            #application des points de dégâts sur la jauge du sorcier 1, appel de perdrePDV()
             self.sorcier[0].perdrePDV(pddSor2)
             print(pddSor2)
             print(self.sorcier[1].getPDV())
             self.score[0]=self.sorcier[0].getPDV()
             self.score[1]=self.sorcier[1].getPDV()
        # Test du cas d'un sort défensif contré par un sort sournois
        elif(typeDeSortSorcier1 =="defensif" and typeDeSortSorcier2=="sournois"):
            # or typeDeSortSorcier1=="surnois"  and typeDeSortSorcier2=="defensif"):
              
            #points de récupération du sort du sorcier 1
            pdrSoc1 =  self.sort[0].getPDR()
            #application des points de récupération sur la jauge du sorcier 1, appel de recupererPDV()
            self.sorcier[0].recupererPDV(pdrSoc1)
            #points de dégats du sort du sorcier 2
            pddSor2 = self.sort[1].getPDD()
            #application des points de dégâts sur la jauge du sorcier 1, appel de perdrePDV()
            self.sorcier[0].perdrePDV(pddSor2)
            print(pddSor1)
            print(self.sorcier[1].getPDV())
            self.score[0]=self.sorcier[0].getPDV()
            self.score[1]=self.sorcier[1].getPDV()
         #enregistrement des points de vie de chaque sorcier dans le tableau de score en fin de tour  
       # self.score[0]=self.sorcier[0].getPDV()
       # self.score[1]=self.sorcier[1].getPDV()
         
    

    def afficherScoreTour(self):
        #affichage du score
        print( "sorcier point de vie ppv1:"+str(self.score[0]))
        print( "sorcier point de vie ppv2:"+str(self.score[1]))
        
    

    def afficherResumeTour(self):
        #affiche : # Tour [Numero du Tour] #
        print(self.numero)
        #appel de afficherScoreTour()
        self.afficherScoreTour()


   
           
